# Android compass app

An compass application for Android phones.

### Installing

Clone the repository and build the app with Android studio.

## Authors

* **Marek Waszczuk** -   [mwaszczuk](https://gitlab.com/mwaszczuk)
