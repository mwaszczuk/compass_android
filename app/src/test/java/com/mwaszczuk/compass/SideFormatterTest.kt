package com.mwaszczuk.compass

import android.content.Context
import com.mwaszczuk.compass.util.components.SideFormatter
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SideFormatterTest {

    @Mock
    private lateinit var mockContext: Context

    @Test
    fun sideFormatterOutput_IsCorrect() {
        `when`(mockContext.getString(R.string.sotw_north)).thenReturn("N")
        `when`(mockContext.getString(R.string.sotw_northeast)).thenReturn("NE")
        `when`(mockContext.getString(R.string.sotw_east)).thenReturn("E")
        `when`(mockContext.getString(R.string.sotw_southeast)).thenReturn("SE")
        `when`(mockContext.getString(R.string.sotw_south)).thenReturn("S")
        `when`(mockContext.getString(R.string.sotw_southwest)).thenReturn("SW")
        `when`(mockContext.getString(R.string.sotw_west)).thenReturn("W")
        `when`(mockContext.getString(R.string.sotw_northwest)).thenReturn("NW")

        val sideFormatter = SideFormatter(mockContext)
        // check if sideFormatter return the sides correctly
        assert(sideFormatter.format(360f).contains("N")) {"SideFormatter didn't return expected value"}
        assert(sideFormatter.format(90f).contains("E")) {"SideFormatter didn't return expected value"}
        assert(sideFormatter.format(180f).contains("S")) {"SideFormatter didn't return expected value"}
        assert(sideFormatter.format(270f).contains("W")) {"SideFormatter didn't return expected value"}
        assert(sideFormatter.format(0f).contains("N")) {"SideFormatter didn't return expected value"}

    }
}