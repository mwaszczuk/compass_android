package com.mwaszczuk.compass

import androidx.multidex.MultiDexApplication
import com.mwaszczuk.compass.util.injection.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class CompassApplication : MultiDexApplication(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()

        if (android.os.Build.VERSION.SDK_INT < 23) {
            setTheme(R.style.AppThemeBelow23)
        }

        AppInjector.init(this)
    }
}