package com.mwaszczuk.compass.util.injection

import dagger.Module

@Module(includes = [ViewModelModule::class])
class AppModule {

}