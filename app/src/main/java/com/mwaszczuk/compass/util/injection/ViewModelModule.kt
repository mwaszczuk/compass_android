package com.mwaszczuk.compass.util.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mwaszczuk.compass.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: CompassViewModelFactory): ViewModelProvider.Factory
}