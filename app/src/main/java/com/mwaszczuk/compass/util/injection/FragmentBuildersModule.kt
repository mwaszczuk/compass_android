package com.mwaszczuk.compass.util.injection

import com.mwaszczuk.compass.ui.coordinates.CoordinatesFragment
import com.mwaszczuk.compass.ui.main.MainFragment
import com.mwaszczuk.compass.ui.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainScreenFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): CoordinatesFragment

    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

}