package com.mwaszczuk.compass.util.components

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.mwaszczuk.compass.R
import com.mwaszczuk.compass.util.extensions.makeGone
import com.mwaszczuk.compass.util.extensions.makeVisible
import kotlinx.android.synthetic.main.component_compass_button.view.*

class CompassButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_compass_button, this, true)

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.CompassButton, defStyle, defStyleRes)
        loadAttributes(attrArray)
    }

    private fun loadAttributes(attrs: TypedArray) {
        buttonText.text = attrs.getText(R.styleable.CompassButton_text)
        if (attrs.getBoolean(R.styleable.CompassButton_isBackButton, false)) {
            imgBack.makeVisible()
            imgNext.makeGone()
        }
        attrs.recycle()

        clickableArea.setOnClickListener { hideKeyboard() }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        clickableArea.setOnClickListener {
            hideKeyboard()
            l?.onClick(clickableArea)
        }
    }

    private fun hideKeyboard() {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)
    }
}
