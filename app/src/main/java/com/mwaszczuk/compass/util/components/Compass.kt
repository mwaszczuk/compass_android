package com.mwaszczuk.compass.util.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Point
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.mwaszczuk.compass.R
import com.mwaszczuk.compass.util.extensions.makeGone
import com.mwaszczuk.compass.util.extensions.makeVisible
import kotlinx.android.synthetic.main.component_compass.view.*
import kotlin.math.cos
import kotlin.math.sin


class Compass @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    private var currentAzimuth = 0.0f
    private var markerAzimuth = 0.0f
    var markerVisible: Boolean = false
    set(value) {
        if (!value) imgMarker.makeGone()
        field = value
    }

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_compass, this, true)
    }

    fun setAzimuth(azimuth: Float) {
        val anim = RotateAnimation(
            -currentAzimuth, -azimuth,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        anim.interpolator = LinearInterpolator()
        anim.duration = 500
        anim.repeatCount = 0
        anim.fillAfter = true
        currentAzimuth = azimuth
        imgMeter.startAnimation(anim)
        frmMarker.startAnimation(anim)
    }

    fun setLocationMarker(azimuth: Double) {
        if (!markerVisible) {
            markerAzimuth = -azimuth.toFloat()
            frmMarker.rotation = -azimuth.toFloat()
            imgMarker.makeVisible()
            markerVisible = true
        }
    }
}
