package com.mwaszczuk.compass.util.injection

import com.mwaszczuk.compass.CompassApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            AndroidSupportInjectionModule::class,
            AppModule::class,
            ActivityModule::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: CompassApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: CompassApplication)

}