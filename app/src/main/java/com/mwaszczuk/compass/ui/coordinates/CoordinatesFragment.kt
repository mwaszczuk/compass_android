package com.mwaszczuk.compass.ui.coordinates

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mwaszczuk.compass.R
import com.mwaszczuk.compass.ui.main.MainViewModel
import com.mwaszczuk.compass.util.injection.CompassViewModelFactory
import com.mwaszczuk.compass.util.injection.Injectable
import kotlinx.android.synthetic.main.fragment_coordinates.*
import javax.inject.Inject

class CoordinatesFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: CompassViewModelFactory
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
         inflater.inflate(R.layout.fragment_coordinates, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnOk.setOnClickListener {
            if (edtLat.text.isNotEmpty() && edtLong.text.isNotEmpty()) {
                viewModel.myLocation.latitude = edtLat.text.toString().toDouble()
                viewModel.myLocation.longitude = edtLong.text.toString().toDouble()
                viewModel.showLocationInfo.value = true
            }
            findNavController().navigateUp()
        }

    }

}
