package com.mwaszczuk.compass.ui.main

import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mwaszczuk.compass.R
import com.mwaszczuk.compass.util.components.SideFormatter
import com.mwaszczuk.compass.util.extensions.makeGone
import com.mwaszczuk.compass.util.extensions.makeVisible
import com.mwaszczuk.compass.util.injection.CompassViewModelFactory
import com.mwaszczuk.compass.util.injection.Injectable
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: CompassViewModelFactory
    lateinit var viewModel: MainViewModel

    private val sideFormatter: SideFormatter by lazy { SideFormatter(requireContext()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
         inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.sensorManager = getSystemService(requireContext(), SensorManager::class.java) as SensorManager

        btnCoordinates.setOnClickListener { findNavController().navigate(MainFragmentDirections.openCoordinates()) }
        btnCancel.setOnClickListener { viewModel.showLocationInfo.value = false }

        observeLiveDatas()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopCompass()
    }

    override fun onResume() {
        super.onResume()
        viewModel.resumeCompass()
    }

    private fun observeLiveDatas() {
        viewModel.azimuth.observe(viewLifecycleOwner, Observer {azimuth ->
            viewModel.showLocationInfo.value?.let {
                if (it) compass.setLocationMarker(viewModel.bearTo.toDouble())
            }
            compass.setAzimuth(azimuth.toFloat())
            txtAzimuth.text = sideFormatter.format(azimuth)
        })

        viewModel.showLocationInfo.observe(viewLifecycleOwner, Observer {
            compass.markerVisible = false
            if (it) {
                viewModel.bearTo = viewModel.myLocation.bearingTo(viewModel.location) + 180
                if (viewModel.bearTo > 360) viewModel.bearTo -= 360
                txtLocation.text = viewModel.myLocation.latitude.toString().plus("  ").plus(viewModel.myLocation.longitude)
                layoutLocation.makeVisible()
            } else {
                layoutLocation.makeGone()
            }
        })
    }

}

