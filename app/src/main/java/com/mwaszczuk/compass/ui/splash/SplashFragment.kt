package com.mwaszczuk.compass.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.mwaszczuk.compass.R
import com.mwaszczuk.compass.util.injection.Injectable

class SplashFragment : Fragment(), Injectable {

    private val handler = Handler()
    private val runnable = Runnable {
        findNavController().navigate(SplashFragmentDirections.openMainFragment())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnable, 1000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

}
