package com.mwaszczuk.compass.ui.main

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(

) : ViewModel(), SensorEventListener {

    var myLocation: Location = Location("default")
    var location: Location = Location("input")
    var bearTo: Float = 0.0f
    val showLocationInfo = MutableLiveData<Boolean>()

    lateinit var sensorManager: SensorManager
    private val accelerometerReading = FloatArray(3)
    private val magnetometerReading = FloatArray(3)
    private val R = FloatArray(9)
    private val I = FloatArray(9)

    val azimuth = MutableLiveData<Float>()
    private val alpha = 0.97f

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        saveReadings(event)
        updateOrientationAngles()
    }

    private fun saveReadings(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            accelerometerReading[0] = alpha * accelerometerReading[0] + (1 - alpha) * event.values[0]
            accelerometerReading[1] = alpha * accelerometerReading[1] + (1 - alpha) * event.values[1]
            accelerometerReading[2] = alpha * accelerometerReading[2] + (1 - alpha) * event.values[2]
        } else if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetometerReading[0] = alpha * magnetometerReading[0] + (1 - alpha) * event.values[0]
            magnetometerReading[1] = alpha * magnetometerReading[1] + (1 - alpha) * event.values[1]
            magnetometerReading[2] = alpha * magnetometerReading[2] + (1 - alpha) * event.values[2]
        }
    }

    private fun updateOrientationAngles() {
        if (SensorManager.getRotationMatrix(R, I, accelerometerReading, magnetometerReading)) {
            val orientation = FloatArray(3)
            SensorManager.getOrientation(R, orientation)
            val mAzimuth = Math.toDegrees(orientation[0].toDouble()).toFloat()
            azimuth.value = (mAzimuth + 360) % 360
        }
    }

    fun stopCompass() {
        sensorManager.unregisterListener(this)
    }

    fun resumeCompass() {
        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)?.also { accelerometer ->
            sensorManager.registerListener(
                this,
                accelerometer,
                SensorManager.SENSOR_DELAY_GAME
            )
        }
        sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)?.also { magneticField ->
            sensorManager.registerListener(
                this,
                magneticField,
                SensorManager.SENSOR_DELAY_GAME
            )
        }
    }
}
